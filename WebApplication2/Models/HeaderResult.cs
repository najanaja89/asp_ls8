﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class HeaderResult : ActionResult
    {
        private readonly string imageName;

        public HeaderResult(string imageName)
        {
            this.imageName = imageName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Write($"<h1>{imageName}</h1>");
        }
    }
}