﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ShopService : IShopService
    {
        private IRepository<Product> repository;

        public ShopService(IRepository<Product> repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Product> GetProductsByCategory(int id)
        {
            return repository.Get().Where(p => p.CategoryId == id);
        }
    }
}