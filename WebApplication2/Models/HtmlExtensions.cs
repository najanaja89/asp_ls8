﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString ErrorLable<TIn>(
            this HtmlHelper html, 
            TIn model, string name) where TIn : class
        {
            var type = model.GetType();
            var propety = type.GetProperty(name);
            var validationAttrs = propety.GetCustomAttributes()
                .ToList()
                .Where(a => a.GetType().BaseType == typeof(ValidationAttribute))
                .Select(a => (ValidationAttribute)a);

            var p = new TagBuilder("p");
            foreach (var attr in validationAttrs)
            {
                p.InnerHtml = attr.ErrorMessage;
            }
            
            return new MvcHtmlString(p.ToString());
        }
    }
}