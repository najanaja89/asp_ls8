﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ProductRepository : IRepository<Product>
    {
        List<Product> products = new List<Product>
        {
            new Product {Id = 1, Name = "Колбаса", Price = 1200, CategoryId = 1},
            new Product {Id = 2, Name = "Сыр", Price = 1000, CategoryId = 2},
            new Product {Id = 3, Name = "Молоко", Price = 300, CategoryId = 2},
        };

        public void Create(Product product)
        {
            products.Add(product);
        }

        public IEnumerable<Product> Get()
        {
            return products;
        }

        public Product GetById(int id)
        {
            return products.SingleOrDefault(p => p.Id == id);
        }
    }
}