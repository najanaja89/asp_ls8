using ConsoleApp1;
using Ninject;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication2.App_Start;
using WebApplication2.Models;

namespace WebApplication2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //var factory = new ServiceFactory(provider =>
            //{
            //    provider.AddTransient<IRepository<Product>, ProductRepository>();
            //    provider.AddTransient<IRepository<Category>, CategoryRepository>();
            //    provider.AddTransient<IShopService, ShopService>();
            //});

            var registrator = new NinjectRegistrator();
            var kernel = new StandardKernel(registrator);
            var resolver = new NinjectDependencyResolver(kernel);
            DependencyResolver.SetResolver(resolver);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_End()
        {

        }

        private void ToDo()
        {

        }
    }
}
