﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using Ninject.Modules;
using WebApplication2.Models;

namespace WebApplication2.App_Start
{
    public class NinjectRegistrator : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository<Product>>().To<ProductRepository>();
            Bind<IShopService>().To<ShopService>();
        }
        
    }
}