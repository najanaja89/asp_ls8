﻿using ConsoleApp1;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    //[RoutePrefix("shop")] //Префикс вместо хоум
    public class HomeController : Controller
    {

        private IShopService service;

        public HomeController(IShopService shopService)
        {
            //service = (new ServiceFactory())
            //    .GetRequriedService<IShopService>();
            service = shopService;
        }
        
        public ActionResult Index()
        {
            //var cookie = HttpContext.Request.Cookies["auth"];
            //if (cookie == null)
            //{
            //    HttpContext.Response.Cookies.Add(new HttpCookie("auth", "User"));
            //}

            ViewBag.user = Session["user"];
            Session.Timeout = 20;
            var newSession = Session.IsNewSession;
            Session.Abandon();
            
            return View();
        }

        [Route("shop/{name}")] //Новый путь shop/{name}
        public ActionResult Products(int? name)
        {
            if (name == null)
                name = 1;

            return View(service.GetProductsByCategory(name.Value));
        }

        void Update(ref Product a)
        {
            a.Name = "";
        }

        // /Home/Product
        [HttpGet]
        public ActionResult Product()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SetLanguage(int id)
        {
            HttpContext.Response.Cookies.Add(new HttpCookie("lang", id.ToString()));
            return new EmptyResult();
        }

        public ActionResult Sidebar()
        {
            ViewBag.Message = "Hello";
            return PartialView();
        }

        [HttpPost]
        public ActionResult Product(Product product)
        {
            if (!ModelState.IsValid)
            {
                return View(product);
            }

            return RedirectToAction("Index");
        }

        public ViewResult About()
        {
            return View();
        }
    }
}