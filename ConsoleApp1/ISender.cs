﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public interface ISender
    {
        void Send(string message);
    }

    public class EmailSender : ISender
    {
        public void Send(string message)
        {
            Console.WriteLine($"E-mail: {message}");
        }
    }

    public class SmsSender : ISender
    {
        public void Send(string message)
        {
            Console.WriteLine($"SMS: {message}");
        }
    }
}
