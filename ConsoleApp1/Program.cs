﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Counter
    {
        public int Count { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ServiceFactory(provider =>
            {
                provider.AddSingleton<Counter>();

                provider.AddTransient<ISender, EmailSender>();
            });




            ISender sender = factory.GetRequriedService<ISender>();
            sender.Send("Hi");

            Counter counter1 = factory.GetRequriedService<Counter>();
            counter1.Count += 5;

            Counter counter2 = factory.GetRequriedService<Counter>();
            Console.WriteLine(counter2.Count);
        }
    }
}
