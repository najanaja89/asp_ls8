﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ServiceProvider
    {
        public static ServiceProvider _instance;
        public static ServiceProvider Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ServiceProvider();

                return _instance;
            }
        }

        Dictionary<Type, Type> _transients;
        Dictionary<Type, object> _singletons;

        private ServiceProvider()
        {
            _transients = new Dictionary<Type, Type>();
            _singletons = new Dictionary<Type, object>();
        }

        public void AddTransient<TType, TImp>() => AddTransient(typeof(TType), typeof(TImp));
        public void AddSingleton<TType, TImp>() => AddSingleton(typeof(TType), typeof(TImp));
        public void AddSingleton<TType>() => AddSingleton(typeof(TType), typeof(TType));


        public object GetService(Type key)
        {
            if (!IsContainsType(key)) 
                return null;

            if (_singletons.ContainsKey(key))
                return _singletons[key];

            Type imp = _transients[key];
            return CreateInstance(imp);
        }


        private void AddTransient(Type key, Type imp)
        {
            if (IsContainsType(key)) return;

            _transients.Add(key, imp);
        }

        private void AddSingleton(Type key, Type imp)
        {
            if (IsContainsType(key)) return;

            var obj = CreateInstance(imp);
            _singletons.Add(key, obj);
        }

        bool IsContainsType(Type type)
        {
            return _transients.ContainsKey(type) || _singletons.ContainsKey(type);
        }

        private object CreateInstance(Type type)
        {
            IEnumerable<object> args = type.GetConstructors()
                .FirstOrDefault()
                .GetParameters()
                .Select(p => GetService(p.ParameterType));

            return Activator.CreateInstance(type, args.ToArray());
        }
    }
}
