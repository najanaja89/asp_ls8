﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ServiceFactory
    {
        public ServiceFactory(){}
        public ServiceFactory(Action<ServiceProvider> regiterServices)
        {
            regiterServices(ServiceProvider.Instance);
        }

        public TType GetRequriedService<TType>() where TType : class
        {
            var service = ServiceProvider.Instance.GetService(typeof(TType));
            if (service is null)
                throw new InvalidOperationException();

            return service as TType;
        }
    }
}
